# Satellite Earth Footprint

Satellite Earth Footprint is a python3 (ported) version of https://github.com/simonaoliver/satellite_earth_footprint

It determines the ground footprint for an earth observing satellite based on a list of satellites, two line element source, observer location, with the option of adding your own Aqusition Of Signal and Loss Of Signal times.

`environment.yml` file has got the project dependencies added to it.

## Prerequisite
- Python 3
- Conda
- PyCharm

### To create a working environment from this repo, please follow the below steps
- Create a new project(Python3) using PyCharm
- Go to terminal tab on PyCharm
- Run command `conda deactivate` - which deactivates virtual environment
- Run command `sudo rm -R .idea/` - clean up default files created by Pycharm
- Run command `git clone git@bitbucket.org:geoscienceaustralia/satellite_earth_footprint.git .`
- Run command `vi environment.yml` and update project in `name:<project>` which is on the first line. `<project>` name should be a new name which was never used before.
- Run command `conda env create -f environment.yml` - installs all the necessary packages
- Run command `conda activate <project>`

### How to execute the script
- Run command `python main.py`

### Other commands
- `condo env export  > environment.yml` - Export environment package information. Make sure this file is thoroughly checked before commit.
- `source activate` - Activates virtual environment
- `conda deactivate` - Deactivates virtual environment
- `conda env list` - Lists all virtual environments

### Note
If you have to update the packages in the environment and export that to `environment.yml`, please do it and when you want to commit it back to git repo, please make sure that `.gitignore` file doesn't include `environment.yml`.
