#!/bin/sh

rm -rf /home/ec2-user/satellite_earth_footprint/output >> /home/ec2-user/satellite_earth_footprint/cronlog.log 2>&1

source /home/ec2-user/miniconda3/bin/activate /home/ec2-user/miniconda3/envs/py37
cd /home/ec2-user/satellite_earth_footprint
python main.py >> /home/ec2-user/satellite_earth_footprint/cronlog.log 2>&1

aws s3 cp output s3://satellite-footprint/output --recursive --acl public-read >> /home/ec2-user/satellite_earth_footprint/cronlog.log 2>&1
